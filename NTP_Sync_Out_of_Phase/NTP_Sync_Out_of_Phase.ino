#include "PowerDue.h"
#include "DueClock.h"
#include "ntp.h"
#include <FreeRTOS_ARM.h>
//#include <IPAddress.h>
#include <PowerDueWiFi.h>


// WiFi network credentials
#define WIFI_SSID "PowerDue"
#define WIFI_PASS "powerdue"


void onError(int errorCode)
{
    SerialUSB.print("Error received: ");
    SerialUSB.println(errorCode);
}

void onReady()
{
    SerialUSB.println("Device ready");
//    SerialUSB.print("Device IP: ");
//    SerialUSB.println(IPAddress(PowerDueWiFi.getDeviceIP()));

    xTaskCreate(ntp_receive_task, "ntp_recv", configMINIMAL_STACK_SIZE * 5, NULL, 4, NULL);

}

void print_time(String tag, DueTime_t *t){
    SerialUSB.print(tag);
    SerialUSB.print(": ");
    SerialUSB.print(t->sec);
    SerialUSB.print("s ");
    SerialUSB.print(t->usec);
    SerialUSB.print("us");
    SerialUSB.println();
}

void print_offset(double offset){
    int positive = 0;
    if(offset >= 0)
        positive = 1;
    else
        offset = -offset;
        
    uint32_t offset_s = (unsigned int) offset;
    uint32_t offset_us = (unsigned int) ((offset - offset_s) * 1000000);

    SerialUSB.print("Offset: ");
    if(positive)
        SerialUSB.print("+ ");
    else
        SerialUSB.print("- ");
    SerialUSB.print(offset_s);
    SerialUSB.print("s ");
    SerialUSB.print(offset_us);
    SerialUSB.print("us");
    SerialUSB.println();
}
double compute_offset(DueTime_t* t0, DueTime_t* t1, DueTime_t* t2, DueTime_t* t3){
    // offset = (t1-t0-t3+t2)/2
    double d_t0 = ((double) t0->usec / 1000000) + t0->sec;
    double d_t1 = ((double) t1->usec / 1000000) + t1->sec;
    double d_t2 = ((double) t2->usec / 1000000) + t2->sec;
    double d_t3 = ((double) t3->usec / 1000000) + t3->sec;
    double offset = (d_t1 - d_t0 - d_t3 + d_t2) / 2;

    return offset;
}

int ntp_build_socket(void){
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_len = sizeof(addr);
    addr.sin_family = AF_INET;
    addr.sin_port = htons(NTP_PORT);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    int sock = lwip_socket(AF_INET, SOCK_DGRAM, 0);
    if(lwip_bind(sock, (struct sockaddr *)&addr, sizeof(addr)) != 0){
        SerialUSB.println("Failed to bind ntp socket");
        while(1);
    }
    return sock;
}

void ntp_send_task(void *arg){
    int socket = (int) arg;
    NtpPacket_t packet;
    DueTime_t t0;
    struct sockaddr_in serverAddr;

    memset(&serverAddr, 0 ,sizeof(serverAddr));
    serverAddr.sin_len = sizeof(serverAddr);
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(NTP_PORT);
    // NTP_SERVER defined as 10.230.0.1 in ntp.h
    inet_pton(AF_INET, NTP_SERVER, &(serverAddr.sin_addr));

    while(1)
    {
        // Sending NTP Request
        Clock.getTime(&t0);     // get current time
        ntp_get_request(&t0, &packet);
        lwip_sendto(socket, &packet, SNTP_MSG_LEN, 0, (const sockaddr *) &serverAddr, sizeof(serverAddr));

        // wait for some amount of time before next request
        // WAITTIME_MS defined in ntp.h
        vTaskDelay(pdMS_TO_TICKS(WAITTIME_MS));
    }
}

void ntp_receive_task(void *arg){
    // Build a UDP socket
    int socket = ntp_build_socket();

    // Kick off an NTP sending task and pass the socket as an arg 
    xTaskCreate(ntp_send_task, "ntp_send", configMINIMAL_STACK_SIZE * 5, (void *)socket, 2, NULL);

    NtpPacket_t packet;
    DueTime_t t0, t1, t2, t3;
    struct sockaddr addr;
    socklen_t socklen;

    while(1){
        memset(&packet, 0, SNTP_MSG_LEN);
        lwip_recvfrom(socket, &packet, SNTP_MSG_LEN, 0, &addr, &socklen);
        // message received from addr

        // get receive time: t3
        Clock.getTime(&t3);

        // extract t0, t1, and t2 form packet
        ntp_get_time_from_packet(&packet, &t0, &t1, &t2);

        print_time("t0", &t0);
        print_time("t1", &t1);
        print_time("t2", &t2);
        print_time("t3", &t3);

        // calc offset
        double offset = compute_offset(&t0, &t1, &t2, &t3);
        print_offset(offset);

        // adjust clock
        Clock.addOffset(offset);
    }
}


void setup() {
    // put your setup code here, to run once:
    PowerDue.LED();
    PowerDue.LED(PD_OFF);

    PIO_Configure(PIOA, PIO_PERIPH_A, PIO_PA2A_TIOA1, PIO_DEFAULT);
    
    SerialUSB.begin(0);
    while(!SerialUSB);
    
    PowerDueWiFi.init(WIFI_SSID, WIFI_PASS);
    PowerDueWiFi.setCallbacks(onReady, onError);
    
    Clock.init();
    Clock.start();

    vTaskStartScheduler();
    SerialUSB.println("Insufficent RAM");
    while(1);
}

void loop() {
    // get and print time every second
    /*
    delay(1000);
    DueTime_t t;
    Clock.getTime(&t);
    SerialUSB.print("Time now is: ");
    SerialUSB.println(t.sec);
    */
}
