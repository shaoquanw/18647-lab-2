#include <Arduino.h>

#if defined(_SAM3XA_)

#include "DueClock.h"

int reset_Rc = 0;
uint32_t adjust_Rc = 0;

void DueClock::init(){
	// disable register write protect to allow TC config
	pmc_set_writeprotect(false);

	// configure a 1Hz clock on TC0-Ch1
	// enable TC0-Ch1 peripheral clock
	pmc_enable_periph_clk(TC1_IRQn);

	// configure the channel mode register to waveform mode
	TC_Configure(TC0, 1,
		TC_CMR_WAVE |					// set TC waveform mode
		TC_CMR_WAVSEL_UP_RC |			// count up with automatic reset when reaching RC value
		TC_CMR_TCCLKS_TIMER_CLOCK4 |	// use Timer Clock 4, MCK/128 ~ 656.25 kHz
		TC_CMR_ACPA_CLEAR |				// set TIOA to LOW when counter value reaches RA
		TC_CMR_ACPC_SET					// set TIOA to HIGH when counter value reaches RC
	);

	// configure the values of RC and RA register
	// ticking @ MCK/128 Hz, then 1s contains MCK/128 ticks
	uint32_t Rc = (VARIANT_MCK/128);	// VARIANT_MCK holds the value of MCK
	TC_SetRC(TC0, 1, Rc);
	TC_SetRA(TC0, 1, Rc/2);		// set RA to be half of Rc to get 50% duty cycle  

	// Enable RA and RC Compare Interrupt
	TC0->TC_CHANNEL[1].TC_IER = (TC_IER_CPAS | TC_IER_CPCS);
	// Disable all other interrupts
	TC0->TC_CHANNEL[1].TC_IDR = ~(TC_IER_CPAS | TC_IER_CPCS);

	// Cler any pending interrupts
	NVIC_ClearPendingIRQ(TC1_IRQn);
	// Enable the Interrupt Vector
	NVIC_EnableIRQ(TC1_IRQn);

	// Clear secondary counter
	_seconds = 0;
}

void DueClock::start(){
	// start TC0. channel 1
	TC_Start(TC0, 1);
}

void DueClock::stop(){
	// stop TC0, channel 1
	TC_Stop(TC0, 1);
}

void DueClock::reset(){
	// Clear secondary counter
	_seconds = 0;
	// calling start again will reset counters to 0
	this->start();
}

void DueClock::getTime(DueTime_t *t){
	t->sec = _seconds;
    uint32_t ticks = TC_ReadCV(TC0, 1);
    uint32_t base = VARIANT_MCK / 128;
    //SerialUSB.println(ticks);
    double percent = (double)ticks / (double)base;
    if(percent >= 0.5)
        percent -= 0.5;
    else
        percent += 0.5; 
    t->usec = (unsigned int)(percent * 1000000);
}

void DueClock::tick(){
	_seconds++;
}

void DueClock::addOffset(double offset){
    _offset = offset;

    if(offset == 0)
        return;
    
    int offset_s = (int) offset;
    _seconds += offset_s;
    double offset_us = offset - offset_s;
    int diff_tick = offset_us * VARIANT_MCK / 128;
    SerialUSB.print("Diff_ticks = ");
    SerialUSB.println(diff_tick);

    
    if(abs(diff_tick) < (VARIANT_MCK / 256)){
        adjust_Rc= (uint32_t) ((int) (VARIANT_MCK / 128) - diff_tick);
    }
    else{
        adjust_Rc= (uint32_t) ((int) (VARIANT_MCK / 128) - diff_tick + (int) (VARIANT_MCK / 128));
        if(offset > 0)
            _seconds++;
        else
            _seconds--;
    }
    SerialUSB.print("Adjust Rc = ");
    SerialUSB.println(adjust_Rc);
}

void TC1_Handler(void){
	// read the status register to clear the interrupt bit
	// and aloow other processing events to proceed
	uint32_t status = TC_GetStatus(TC0, 1);

	// check if we've reached RA or RC avlue
	// LED will blink along with the 1Hz square wave
	if(status & TC_SR_CPAS)		// counter reaches RA value
	{
		PowerDue.LED(PD_TIEL);
	}
	else if(status & TC_SR_CPCS)	// counter reaches RC value
	{
		PowerDue.LED(PD_OFF);
		// increment second counter to keep track of passed second
		Clock.tick();
        if(adjust_Rc != 0)
        {
            TC_SetRC(TC0, 1, adjust_Rc);
            adjust_Rc = 0;
            reset_Rc = 1;
        }
        else if(reset_Rc)
        {
            uint32_t Rc = (VARIANT_MCK/128);
            TC_SetRC(TC0, 1, Rc);
            reset_Rc = 0;
        }
	}
}

DueClock Clock = DueClock();

#endif
