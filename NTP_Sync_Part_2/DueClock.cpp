#include <Arduino.h>

#if defined(_SAM3XA_)

#include "DueClock.h"

int reset_Rc = 0;
uint32_t adjust_Rc = 0;

void DueClock::init(){
	// disable register write protect to allow TC config
	pmc_set_writeprotect(false);

	// configure a 1Hz clock on TC0-Ch1
	// enable TC0-Ch1 peripheral clock
	pmc_enable_periph_clk(TC1_IRQn);

	// configure the channel mode register to waveform mode
	TC_Configure(TC0, 1,
		TC_CMR_WAVE |					// set TC waveform mode
		TC_CMR_WAVSEL_UP_RC |			// count up with automatic reset when reaching RC value
		TC_CMR_TCCLKS_TIMER_CLOCK4 |	// use Timer Clock 4, MCK/128 ~ 656.25 kHz
		TC_CMR_ACPA_CLEAR |				// set TIOA to LOW when counter value reaches RA
		TC_CMR_ACPC_SET					// set TIOA to HIGH when counter value reaches RC
	);

	// configure the values of RC and RA register
	// ticking @ MCK/128 Hz, then 1s contains MCK/128 ticks
	uint32_t Rc = (VARIANT_MCK/128);	// VARIANT_MCK holds the value of MCK
	TC_SetRC(TC0, 1, Rc);
	TC_SetRA(TC0, 1, Rc/2);		// set RA to be half of Rc to get 50% duty cycle  

	// Enable RA and RC Compare Interrupt
	TC0->TC_CHANNEL[1].TC_IER = (TC_IER_CPAS | TC_IER_CPCS);
	// Disable all other interrupts
	TC0->TC_CHANNEL[1].TC_IDR = ~(TC_IER_CPAS | TC_IER_CPCS);

	// Cler any pending interrupts
	NVIC_ClearPendingIRQ(TC1_IRQn);
	// Enable the Interrupt Vector
	NVIC_EnableIRQ(TC1_IRQn);

	// Clear secondary counter
	_seconds = 0;
}

void DueClock::start(){
	// start TC0. channel 1
	TC_Start(TC0, 1);
}

void DueClock::stop(){
	// stop TC0, channel 1
	TC_Stop(TC0, 1);
}

void DueClock::reset(){
	// Clear secondary counter
	_seconds = 0;
	// calling start again will reset counters to 0
	this->start();
}

void DueClock::getTime(DueTime_t *t){
	t->sec = _seconds;
    uint32_t ticks = TC_ReadCV(TC0, 1);
    uint32_t base = VARIANT_MCK / 128;
    //SerialUSB.println(ticks);
    double percent = (double)ticks / (double)base;

    // RA = Falling Edge; RC = Raising Edge in this implementation
    // Un-commented the following block to switch to
    // RA = Raising Edge; RC = Falling Edge case
    /*
    if(percent >= 0.5)
        percent -= 0.5;
    else
        percent += 0.5; 
    */
    t->usec = (unsigned int)(percent * 1000000);
}

void DueClock::tick(){
	_seconds++;
}

void DueClock::addOffset(double offset){
    _offset = offset;

    if(offset == 0)
        return;

    // get integer part of the offset, adjust _seconds accordingly
    int offset_s = (int) offset;
    _seconds += offset_s;

    // get fraction part of the offset, convert delay from us to ticks
    double offset_us = offset - offset_s;
    int diff_tick = offset_us * VARIANT_MCK / 128;

    /*
    SerialUSB.print("Diff_ticks = ");
    SerialUSB.println(diff_tick);
    */

    /* adjust phase by shifting cycle length */
    // Find the closest way to align the phase
    // For example, +0.90 -> +1.00 - 0.10; +0.40 -> +0.00 + 0.40;
    // In another word, try to move the phase for less than half of a cycle 
    if(abs(diff_tick) < (VARIANT_MCK / 256)){
        // Case 1: the difference is less than half of a cycle
        // if diff_tick is positive, ref clock has a larger clock number
        // we need to end a cycle earlier to catch up with the ref
        // which leads to subtracting Rc by diff_ticks.
        // the negative case works similarly
        adjust_Rc= (uint32_t) ((int) (VARIANT_MCK / 128) - diff_tick);
    }
    else{
        // Case 2: the difference is more than half of a cycle
        // then we'll shift it the other way, and adjust _seconds value accordingly
        // basically, diff_ticks > 0.5 cycle -> (cycle - diff_ticks) < 0.5 cycle
        // then  move it the other way: 2cycle - diff_ticks
        adjust_Rc= (uint32_t) ((int) (VARIANT_MCK / 128) - diff_tick + (int) (VARIANT_MCK / 128));
        if(offset > 0)
            _seconds++;
        else
            _seconds--;
    }
    /*
    SerialUSB.print("Adjust Rc = ");
    SerialUSB.println(adjust_Rc);
    */

    /* Compliment RC is calculated here, but actual adjustment happens later, next time in RC intterupt */
}

void TC1_Handler(void){
	// read the status register to clear the interrupt bit
	// and aloow other processing events to proceed
	uint32_t status = TC_GetStatus(TC0, 1);

	// check if we've reached RA or RC avlue
	// LED will blink along with the 1Hz square wave
	if(status & TC_SR_CPAS)		// counter reaches RA value
	{
		PowerDue.LED(PD_OFF);
	}
	else if(status & TC_SR_CPCS)	// counter reaches RC value
	{
		PowerDue.LED(PD_TIEL);
		// increment second counter to keep track of passed second
		Clock.tick();

        // reached RC interrupt, check if adjustment needs to be taken
        if(adjust_Rc != 0)
        {
            // adjust_Rc not equal to 0, change Rc to this value,
            // and set reset_Rc to reset clock to original value the next cycle
            TC_SetRC(TC0, 1, adjust_Rc);
            adjust_Rc = 0;
            reset_Rc = 1;
        }
        else if(reset_Rc)
        {
            // we adjusted Rc to sync the clock phase last cycle,
            // reset it to maintain 1s cycle again
            uint32_t Rc = (VARIANT_MCK/128);
            TC_SetRC(TC0, 1, Rc);
            reset_Rc = 0;
        }
	}
}

DueClock Clock = DueClock();

#endif
