#ifndef __CLOCK_H
#define __CLOCK_H

#if defined(_SAM3XA_)
#include "PowerDue.h"

#include "DueTime.h"

class DueClock {
public:
  void init();
  
  void start();
  void stop();
  void reset();
  
  void getTime(DueTime_t *t);
  void tick();

private:
  uint32_t _seconds = 0;
};

extern DueClock Clock;

#else

#error "Are you compiling for the PowerDue?"

#endif

#endif
