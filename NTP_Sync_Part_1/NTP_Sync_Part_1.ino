#include "PowerDue.h"
#include "DueClock.h"
void setup() {
    // put your setup code here, to run once:
    PowerDue.LED();
    PowerDue.LED(PD_OFF);

    PIO_Configure(PIOA, PIO_PERIPH_A, PIO_PA2A_TIOA1, PIO_DEFAULT);
    
    SerialUSB.begin(0);
    while(!SerialUSB);

    SerialUSB.println("Serial open");

    Clock.init();
    Clock.start();
    SerialUSB.println("Clock ready");
}

void loop() {
    // get and print time every second
    delay(1000);
    DueTime_t t;
    Clock.getTime(&t);
    SerialUSB.print("Time now is: ");
    SerialUSB.println(t.sec);
}
